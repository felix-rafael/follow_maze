package server_test

import (
	"testing"
	"time"

	"soundcloud.com/follower_maze/internal/server"
)

// Testing Event parsing
func TestParseASuccessfullEvent(t *testing.T) {
	event, err := server.ParseEvent("666|F|60|50")

	if err != nil {
		t.Error(err)
	}

	if event.Sequence != 666 {
		t.Errorf("Expecting sequence to be 666 but it was %d", event.Sequence)
	}

	if event.Type != "F" {
		t.Errorf("Expecting type to be F but it was %v", event.Type)
	}

	if event.FromUserID != "60" {
		t.Errorf("Expecting FromUserID to be 60 but it was %v", event.FromUserID)
	}

	if event.ToUserID != "50" {
		t.Errorf("Expecting ToUserId to be 50 but it was %v", event.ToUserID)
	}
}

func TestParseASuccessfullBroadcastEvent(t *testing.T) {
	event, err := server.ParseEvent("542532|B")

	if err != nil {
		t.Error(err)
	}

	if event.Sequence != 542532 {
		t.Errorf("Expecting sequence to be 666 but it was %d", event.Sequence)
	}

	if event.Type != "B" {
		t.Errorf("Expecting type to be B but it was %v", event.Type)
	}
}

func TestParseASuccessfullStatusUpdateEvent(t *testing.T) {
	event, err := server.ParseEvent("634|S|32")

	if err != nil {
		t.Error(err)
	}

	if event.Sequence != 634 {
		t.Errorf("Expecting sequence to be 634 but it was %d", event.Sequence)
	}

	if event.Type != "S" {
		t.Errorf("Expecting type to be S but it was %v", event.Type)
	}

	if event.FromUserID != "32" {
		t.Errorf("Expecting FromUserID to be 32 but it was %v", event.FromUserID)
	}
}

func TestParseAnInvalidEvent(t *testing.T) {
	_, err := server.ParseEvent("5425FOO|B")

	if err == nil {
		t.Error("Expecting error to be raised, but it was successful")
	}
}

// Testing Handler
func TestStoringNewIncomingEvents(t *testing.T) {
	input := make(chan server.Event)
	handler := server.NewEventHandler(input, &fakeLogger{})
	defer handler.Stop()
	handler.Start()

	input <- server.Event{Sequence: 100, Type: "B"}
	time.Sleep(5 * time.Millisecond)

	event := handler.Events[100]

	if event.Type != "B" {
		t.Error("Event not found")
	}
}

func TestProcessingOutgoingEvents(t *testing.T) {
	input := make(chan server.Event)
	handler := server.NewEventHandler(input, &fakeLogger{})
	defer handler.Stop()
	output := handler.Start()

	// The listener should receive these events in order
	input <- server.Event{Sequence: 3, Type: "B"}
	input <- server.Event{Sequence: 1, Type: "F"}
	input <- server.Event{Sequence: 2, Type: "S"}

	event1 := <-output
	event2 := <-output
	event3 := <-output

	if event1.Sequence != 1 {
		t.Errorf("First event should have sequence 1, but it was %d", event1.Sequence)
	}

	if event2.Sequence != 2 {
		t.Errorf("Second event should have sequence 2, but it was %d", event2.Sequence)
	}

	if event3.Sequence != 3 {
		t.Errorf("Third event should have sequence 3, but it was %d", event3.Sequence)
	}
}
