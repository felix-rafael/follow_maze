package server

import (
	"bufio"
	"fmt"
	"net"
	"strings"
	"sync"
	"time"
)

type client struct {
	ID        string
	conn      net.Conn
	followers map[string]bool
}

func (c *client) Notify(event Event) {
	if c.conn == nil {
		return
	}

	c.conn.Write([]byte(event.ToPayload() + "\n"))
}

func (c *client) HasFollower(ID string) bool {
	_, ok := c.followers[ID]

	return ok
}

type clientServer struct {
	address     string
	Clients     map[string]client
	log         logger
	lock        sync.RWMutex
	events      <-chan Event
	stopCommand bool
}

func (c *clientServer) addClient(ID string, conn net.Conn) client {
	c.lock.Lock()
	cl, ok := c.Clients[ID]
	if !ok {
		cl = client{
			ID:        ID,
			followers: make(map[string]bool),
		}
	}
	cl.conn = conn
	c.Clients[ID] = cl
	c.lock.Unlock()

	return cl
}

func (c *clientServer) listen() error {
	listener, err := net.Listen("tcp", c.address)
	if err != nil {
		c.log.Printf("Faield to start client server: %v", err)
		return err
	}
	defer listener.Close()
	c.log.Printf("ClientServer listening at %v\n", c.address)

	for {
		if c.stopCommand {
			c.log.Println("ClientServer stopping...")
			break
		}

		conn, err := listener.Accept()
		if err == nil {
			// Setup timeouts for Read
			conn.SetReadDeadline(time.Now().Add(15 * time.Millisecond))
			reader := bufio.NewReader(conn)
			message, err := reader.ReadString('\n')
			if err != nil {
				c.log.Println("Client did not send identification in time")
				conn.Close()
				continue
			}

			client := c.addClient(strings.TrimSuffix(message, "\n"), conn)
			c.log.Printf("client %v connected\n", client.ID)
		}
	}

	return nil
}

func (c *clientServer) broadcast(event Event) {
	for _, client := range c.Clients {
		client.Notify(event)
	}
}

func (c *clientServer) privateMessage(event Event) {
	toClient, ok := c.Clients[event.ToUserID]
	if !ok {
		return
	}

	toClient.Notify(event)
}

func (c *clientServer) follow(event Event) {
	toClient, ok := c.Clients[event.ToUserID]
	if !ok {
		toClient = c.addClient(event.ToUserID, nil)
	}

	toClient.followers[event.FromUserID] = true
	toClient.Notify(event)
}

func (c *clientServer) unfollow(event Event) {
	toClient, ok := c.Clients[event.ToUserID]
	if !ok {
		return
	}

	delete(toClient.followers, event.FromUserID)
}

func (c *clientServer) statusUpdate(event Event) {
	fromClient, ok := c.Clients[event.FromUserID]
	if !ok {
		return
	}

	for clientID := range fromClient.followers {
		client, ok := c.Clients[clientID]
		if !ok {
			continue
		}
		client.Notify(event)
	}
}

func (c *clientServer) handleMessages() {
	for {
		event := <-c.events
		switch event.Type {
		case "B":
			c.broadcast(event)
		case "P":
			c.privateMessage(event)
		case "F":
			c.follow(event)
		case "U":
			c.unfollow(event)
		case "S":
			c.statusUpdate(event)
		}
	}
}

func (c *clientServer) Start() {
	c.stopCommand = false
	go c.listen()
	go c.handleMessages()
}

func (c *clientServer) Stop() {
	c.stopCommand = true
	for _, client := range c.Clients {
		if client.conn == nil {
			continue
		}

		client.conn.Close()
	}
}

func NewClientServer(events <-chan Event, port int, log logger) clientServer {
	return clientServer{
		address: fmt.Sprintf("localhost:%v", port),
		log:     log,
		Clients: make(map[string]client),
		events:  events,
	}
}
