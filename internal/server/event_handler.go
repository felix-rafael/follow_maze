package server

import (
	"strconv"
	"strings"
	"sync"
)

type Event struct {
	Sequence   int
	Type       string
	FromUserID string
	ToUserID   string
}

func (e *Event) ToPayload() string {
	elements := strings.Join([]string{strconv.Itoa(e.Sequence), e.Type, e.FromUserID, e.ToUserID}, " ")

	return strings.Join(strings.Fields(elements), "|")
}

type eventHandler struct {
	Events map[int]Event
	output chan Event
	input  chan Event
	log    logger
	lock   sync.RWMutex
}

func (r *eventHandler) handleOutgoingEvents() {
	index := 1
	for {
		r.lock.RLock()
		event, ok := r.Events[index]
		r.lock.RUnlock()
		if !ok {
			continue
		}

		r.output <- event
		index = index + 1
	}
}

func (r *eventHandler) handleIncomingEvents() {
	for {
		newEvent := <-r.input
		r.lock.Lock()
		r.Events[newEvent.Sequence] = newEvent
		r.lock.Unlock()
	}
}

func (r *eventHandler) Start() chan Event {
	go r.handleIncomingEvents()
	go r.handleOutgoingEvents()

	return r.output
}

func (r *eventHandler) Stop() {
	close(r.output)
}

func NewEventHandler(input chan Event, log logger) *eventHandler {
	return &eventHandler{input: input, output: make(chan Event), Events: make(map[int]Event), log: log}
}

func ParseEvent(rawEvent string) (Event, error) {
	elements := strings.Split(rawEvent, "|")
	seq, err := strconv.Atoi(elements[0])
	if err != nil {
		return Event{}, err
	}
	eventType := elements[1]
	var fromUserID string
	var toUserID string
	if len(elements) >= 3 {
		fromUserID = elements[2]
	}
	if len(elements) == 4 {
		toUserID = elements[3]
	}

	return Event{seq, eventType, fromUserID, toUserID}, nil
}
