package server_test

import (
	"net"
	"testing"
	"time"

	"soundcloud.com/follower_maze/internal/server"
)

func TestClientConnectingSuccesfully(t *testing.T) {
	clientServer := server.NewClientServer(make(chan server.Event), 9099, &fakeLogger{})
	clientServer.Start()
	defer clientServer.Stop()

	conn, err := net.Dial("tcp", "localhost:9099")
	if err != nil {
		t.Fatal("Failed to connect to test server")
	}

	message := "1892"
	conn.Write([]byte(message + "\n"))
	conn.Close()

	// Wait for server
	time.Sleep(10 * time.Millisecond)

	if len(clientServer.Clients) == 0 {
		t.Error("Client was not connected")
	}

	_, ok := clientServer.Clients[message]
	if !ok {
		t.Errorf("TestClientConnectingSuccesfully expected ID %v exists", message)
	}
}

func TestClientFailedToSendId(t *testing.T) {
	clientServer := server.NewClientServer(make(chan server.Event), 9099, &fakeLogger{})
	clientServer.Start()
	defer clientServer.Stop()

	_, err := net.Dial("tcp", "localhost:9099")
	if err != nil {
		t.Fatal("Failed to connect to test server")
	}

	// Wait for server
	time.Sleep(15 * time.Millisecond)
	if len(clientServer.Clients) != 0 {
		t.Error("Client should timeout")
	}
}
