package server

type logger interface {
	Printf(format string, v ...interface{})
	Println(v ...interface{})
}
