package server_test

import (
	"net"
	"testing"

	"soundcloud.com/follower_maze/internal/server"
)

type fakeLogger struct{}

func (fl *fakeLogger) Printf(format string, v ...interface{}) {}
func (fl *fakeLogger) Println(v ...interface{})               {}

func TestClientSendsAnEvent(t *testing.T) {
	consumer := server.NewConsumer(9090, &fakeLogger{})
	defer consumer.Stop()
	repository := consumer.Start()

	conn, err := net.Dial("tcp", "localhost:9090")
	if err != nil {
		t.Fatal("Failed to connect to test server")
	}

	message := "666|F|60|50\n"
	conn.Write([]byte(message))
	conn.Close()

	event := <-repository

	if event.Sequence != 666 {
		t.Errorf("TestClientSendsAnEvent expected last message to be %q but got %q", message, event)
	}
}
