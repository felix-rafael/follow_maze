package server

import (
	"bufio"
	"fmt"
	"net"
	"strings"
)

type eventSource struct {
	conn   net.Conn
	server *consumerServer
}

func (es *eventSource) listen() {
	reader := bufio.NewReader(es.conn)
	for {
		message, err := reader.ReadString('\n')
		if err != nil {
			es.conn.Close()
			return
		}

		es.server.processMessage(es, message)
	}
}

type consumerServer struct {
	address     string
	repository  chan Event
	log         logger
	stopCommand bool
	sources     []eventSource
}

func (c *consumerServer) listen() error {
	c.stopCommand = false
	listener, err := net.Listen("tcp", c.address)
	if err != nil {
		c.log.Printf("Faield to start event consumer: %v", err)
		return err
	}
	defer listener.Close()
	c.log.Printf("EventConsumer listening at %v\n", c.address)

	for {
		if c.stopCommand {
			c.log.Println("EventConsumer stopping...")
			break
		}

		conn, err := listener.Accept()
		if err == nil {
			eventSource := eventSource{
				conn:   conn,
				server: c,
			}
			c.sources = append(c.sources, eventSource)
			c.log.Println("event source connected, receiving events")
			go eventSource.listen()
		}
	}

	return nil
}

func (c *consumerServer) Start() chan Event {
	go c.listen()

	return c.repository
}

func (c *consumerServer) Stop() {
	c.stopCommand = true
	close(c.repository)

	for _, source := range c.sources {
		source.conn.Close()
	}
}

func (c *consumerServer) processMessage(source *eventSource, message string) {
	event, err := ParseEvent(strings.TrimSuffix(message, "\n"))
	if err != nil {
		c.log.Printf("Failed to decode event %v", message)
		return
	}

	c.repository <- event
}

func NewConsumer(port int, log logger) consumerServer {
	return consumerServer{
		repository: make(chan Event),
		address:    fmt.Sprintf("localhost:%v", port),
		log:        log,
	}
}
