package server_test

import (
	"bufio"
	"net"
	"testing"
	"time"

	"soundcloud.com/follower_maze/internal/server"
)

func TestHandlingABroadcastMessage(t *testing.T) {
	events := make(chan server.Event)
	clientServer := server.NewClientServer(events, 9099, &fakeLogger{})
	clientServer.Start()
	defer clientServer.Stop()

	conn, err := net.Dial("tcp", "localhost:9099")
	if err != nil {
		t.Fatal("Failed to connect to test server")
	}
	defer conn.Close()

	conn.Write([]byte("192\n"))
	time.Sleep(5 * time.Millisecond)

	events <- server.Event{Sequence: 2131, Type: "B"}
	conn.SetReadDeadline(time.Now().Add(10 * time.Millisecond))
	reader := bufio.NewReader(conn)
	receivedMessage, err := reader.ReadString('\n')
	if err != nil {
		t.Error(err)
	}

	expected := "2131|B\n"
	if receivedMessage != expected {
		t.Errorf("Expecting message %v but received %v", expected, receivedMessage)
	}
}

func TestHandlingAPrivateMessage(t *testing.T) {
	events := make(chan server.Event)
	clientServer := server.NewClientServer(events, 9098, &fakeLogger{})
	clientServer.Start()
	defer clientServer.Stop()

	fromConn, err := net.Dial("tcp", "localhost:9098")
	if err != nil {
		t.Fatal("Failed to connect to test server")
	}
	fromConn.Write([]byte("18\n"))
	fromConn.Close()

	conn, err := net.Dial("tcp", "localhost:9098")
	if err != nil {
		t.Fatal("Failed to connect to test server")
	}
	defer conn.Close()
	conn.Write([]byte("20\n"))
	time.Sleep(5 * time.Millisecond)

	events <- server.Event{Sequence: 201, Type: "P", FromUserID: "18", ToUserID: "20"}
	conn.SetReadDeadline(time.Now().Add(10 * time.Millisecond))
	reader := bufio.NewReader(conn)
	receivedMessage, err := reader.ReadString('\n')
	if err != nil {
		t.Error(err)
	}

	expected := "201|P|18|20\n"
	if receivedMessage != expected {
		t.Errorf("Expecting message %v but received %v", expected, receivedMessage)
	}
}

func TestHandlingAFollowMessage(t *testing.T) {
	events := make(chan server.Event)
	clientServer := server.NewClientServer(events, 9097, &fakeLogger{})
	clientServer.Start()
	defer clientServer.Stop()

	conn, err := net.Dial("tcp", "localhost:9097")
	if err != nil {
		t.Fatal("Failed to connect to test server")
	}
	defer conn.Close()
	conn.Write([]byte("20\n"))
	time.Sleep(5 * time.Millisecond)

	events <- server.Event{Sequence: 201, Type: "F", FromUserID: "18", ToUserID: "20"}
	conn.SetReadDeadline(time.Now().Add(10 * time.Millisecond))
	reader := bufio.NewReader(conn)
	receivedMessage, err := reader.ReadString('\n')
	if err != nil {
		t.Error(err)
	}

	expected := "201|F|18|20\n"
	if receivedMessage != expected {
		t.Errorf("Expecting message %v but received %v", expected, receivedMessage)
	}

	client := clientServer.Clients["20"]
	if !client.HasFollower("18") {
		t.Error("Expecting client 20 to have client 18 as follower")
	}
}

func TestHandlingAFollowMessageWhenToClientIsNotConnected(t *testing.T) {
	events := make(chan server.Event)
	clientServer := server.NewClientServer(events, 9096, &fakeLogger{})
	clientServer.Start()
	defer clientServer.Stop()

	events <- server.Event{Sequence: 201, Type: "F", FromUserID: "18", ToUserID: "20"}

	_, ok := clientServer.Clients["20"]
	if !ok {
		t.Error("Expecting client 20 to exists")
	}

	client := clientServer.Clients["20"]
	if !client.HasFollower("18") {
		t.Error("Expecting client 20 to have client 18 as follower")
	}
}

func TestHandlingAnUnfollowMessage(t *testing.T) {
	events := make(chan server.Event)
	clientServer := server.NewClientServer(events, 9097, &fakeLogger{})
	clientServer.Start()
	defer clientServer.Stop()

	conn, err := net.Dial("tcp", "localhost:9097")
	if err != nil {
		t.Fatal("Failed to connect to test server")
	}
	defer conn.Close()
	conn.Write([]byte("20\n"))
	time.Sleep(5 * time.Millisecond)

	events <- server.Event{Sequence: 201, Type: "F", FromUserID: "18", ToUserID: "20"}
	time.Sleep(5 * time.Millisecond)
	events <- server.Event{Sequence: 205, Type: "U", FromUserID: "18", ToUserID: "20"}
	time.Sleep(5 * time.Millisecond)

	client := clientServer.Clients["20"]
	if client.HasFollower("18") {
		t.Error("Expecting client 20 to not have client 18 as follower")
	}
}

func TestHandlingAStatusUpdatewMessage(t *testing.T) {
	events := make(chan server.Event)
	clientServer := server.NewClientServer(events, 9095, &fakeLogger{})
	clientServer.Start()
	defer clientServer.Stop()

	fromConn, err := net.Dial("tcp", "localhost:9095")
	if err != nil {
		t.Fatal("Failed to connect to test server")
	}
	fromConn.Write([]byte("18\n"))
	defer fromConn.Close()

	conn, err := net.Dial("tcp", "localhost:9095")
	if err != nil {
		t.Fatal("Failed to connect to test server")
	}
	defer conn.Close()
	conn.Write([]byte("20\n"))
	time.Sleep(5 * time.Millisecond)

	events <- server.Event{Sequence: 201, Type: "F", FromUserID: "18", ToUserID: "20"}
	events <- server.Event{Sequence: 208, Type: "F", FromUserID: "50", ToUserID: "20"}
	time.Sleep(5 * time.Millisecond)
	events <- server.Event{Sequence: 210, Type: "S", FromUserID: "20"}
	time.Sleep(5 * time.Millisecond)

	fromConn.SetReadDeadline(time.Now().Add(10 * time.Millisecond))
	reader := bufio.NewReader(fromConn)
	receivedMessage, err := reader.ReadString('\n')
	if err != nil {
		t.Error(err)
	}

	expected := "210|S|20\n"
	if receivedMessage != expected {
		t.Errorf("Expecting message %v but received %v", expected, receivedMessage)
	}
}
