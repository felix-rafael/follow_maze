package main

import (
	"log"
	"os"
	"os/signal"
	"syscall"

	"soundcloud.com/follower_maze/internal/server"
)

func main() {
	log := log.New(os.Stdout, "", log.LstdFlags|log.Lshortfile)

	eventConsumer := server.NewConsumer(9090, log)
	defer eventConsumer.Stop()
	inputEventChannel := eventConsumer.Start()

	eventHandler := server.NewEventHandler(inputEventChannel, log)
	defer eventHandler.Stop()
	outputEventChannel := eventHandler.Start()

	clientServer := server.NewClientServer(outputEventChannel, 9099, log)
	defer clientServer.Stop()
	clientServer.Start()

	c := make(chan os.Signal)
	signal.Notify(c, os.Interrupt, syscall.SIGTERM)
	<-c
}
