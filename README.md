# Follower Maze
My approach to solve the follower maze challenge in Go language.

## Configuration

The app is written using only go 1.12.5 for mac, only standard library.
The project was tested inside the **$GOPATH**, please make sure that the projects lives under **$GOPATH/src/soundcloud.com/follower_maze**

Running tests
```bash
go test -v ./...
```

Running the app
```bash
go run cmd/follower_maze/main.go
```

## How it works

There are two server running, one that expect the event source to connect and one that expect the users.

![appflow](docs/flow.jpg)

### EventConsumer
This is the server where event source is expected to connect, the server operates with a channel and every message that is received from the tcp socket is parsed into a higher level `Event` object and sent to this channel.

### ClientServer
When started it has two responsabilities, a tcp server for clients and handle events to be distributed to the clients.

#### TCP Server
Listen to any connections to the given port, a client fails to connect when it does not sent its identification in less than 15ms.

#### Event processing
ClientServer has a channel where events are suppose to arrive, every event that arrive to this channel is processed accordingly, there is no validation, it assumes that the events here are safe to be used.

### EventHandler
This is an intermediate layer, which takes the input channel with the raw messages sent by the `EventConsumer` and organizes/validates them before distributing to an output channel.

## Notes and comments
There are some issues specially when handling the port on tests, sometimes the server takes longer to shutdown in tests thus causing error, this was fixed using a different port in some of the test cases.
The issue that took me longer to realize was that some `Follow` events arrived before the client being connected, therefore some clients would timeout waiting for a discarded message, to fix it I added the client even without a valid connection, if the connection appears the user gets notified.
Seems that **go** does not have a way to check if the connections are still active, therefore after the each run the server needs to be restarted, otherwise clients would get random timeouts due to trying to access some stale connection.
Also using buffered channels also would add some back-pressure mechanism.
